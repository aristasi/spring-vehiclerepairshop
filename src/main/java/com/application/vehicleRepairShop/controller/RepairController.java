package com.application.vehicleRepairShop.controller;

import com.application.vehicleRepairShop.domain.Part;
import com.application.vehicleRepairShop.domain.Repair;
import com.application.vehicleRepairShop.domain.Vehicle;
import com.application.vehicleRepairShop.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/repairs")
public class RepairController {

    @Autowired
    RepairService repairService;

    @PostMapping
    public Repair insert(@RequestBody Repair repair) {
        return repairService.save(repair);
    }

    @PutMapping
    public Repair update(@RequestBody Repair repair){
        return repairService.save(repair);
    }

    @DeleteMapping("/{id}/delete")
    public void delete(@PathVariable(value = "id") long id){
        repairService.deleteById(id);
    }

    @GetMapping
    public List<Repair> findAll(){
        return repairService.findAll();
    }

    @GetMapping("/{id}")
    public Repair findById(@PathVariable(value = "id") long id){
        return repairService.findById(id);
    }

    @GetMapping("/vehicleid/{vehicleid}")
    public List<Repair> findByVehicleId(@PathVariable(value = "vehicleid") long id){
        return repairService.findByVehicleId(id);
    }

}
