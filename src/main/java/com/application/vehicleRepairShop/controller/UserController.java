package com.application.vehicleRepairShop.controller;


import com.application.vehicleRepairShop.domain.User;
import com.application.vehicleRepairShop.domain.Vehicle;
import com.application.vehicleRepairShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.application.vehicleRepairShop.utilities.UserType.ADMIN;
import static com.application.vehicleRepairShop.utilities.UserType.USER;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User insert(@RequestBody User user) {
        return userService.save(user);
    }

    @PutMapping
    public User update(@RequestBody User user){
        return userService.save(user);
    }

    @DeleteMapping("/{id}/delete")
    public void delete(@PathVariable(value = "id") long id){
        userService.deleteById(id);
    }

    @GetMapping
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable(value = "id") long id){
        return userService.findById(id);
    }

    @GetMapping(path = "/email/{email}")
    public User findByEmail(@PathVariable(value = "email") String email){
        return userService.findByEmail(email);
    }

}
