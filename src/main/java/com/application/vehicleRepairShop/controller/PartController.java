package com.application.vehicleRepairShop.controller;

import com.application.vehicleRepairShop.domain.Part;
import com.application.vehicleRepairShop.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(path = "/parts")
public class PartController {

    @Autowired
    PartService partService;

    @PostMapping
    public Part insert(@RequestBody Part part) {
        return partService.save(part);
    }

    @PutMapping
    public Part update(@RequestBody Part part){
        return partService.save(part);
    }

    @DeleteMapping("/{id}/delete")
    public void delete(@PathVariable(value = "id") long id){
        partService.deleteById(id);
    }

    @GetMapping
    public List<Part> findAll(){
        return partService.findAll();
    }

    @GetMapping("/{id}")
    public Part findById(@PathVariable(value = "id") long id){
        return partService.findById(id);
    }

    @GetMapping("/type/{type}")
    public List<Part> findByType(@PathVariable(value = "type") String type){
        return partService.findByType(type);
    }

}
