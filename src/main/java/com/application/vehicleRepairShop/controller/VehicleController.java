package com.application.vehicleRepairShop.controller;

import com.application.vehicleRepairShop.domain.Repair;
import com.application.vehicleRepairShop.domain.User;
import com.application.vehicleRepairShop.domain.Vehicle;
import com.application.vehicleRepairShop.service.VehicleService;
import com.application.vehicleRepairShop.service.VehicleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(path = "/vehicles")
public class VehicleController {

    @Autowired
    VehicleService vehicleService;

    @PostMapping
    public Vehicle insert(@RequestBody Vehicle vehicle) {
        return vehicleService.save(vehicle);
    }

    @PutMapping
    public Vehicle update(@RequestBody Vehicle vehicle){
        return vehicleService.save(vehicle);
    }

    @DeleteMapping("/{id}/delete")
    public void deleteById(@PathVariable(value = "id") long id){
        vehicleService.deleteById(id);
    }

    @GetMapping
    public List<Vehicle> findAll(){
        return vehicleService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Vehicle> findById(@PathVariable(value = "id") long id){
        Vehicle vehicle = vehicleService.findById(id);

        if(vehicle==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(null);
        }

        return ResponseEntity.status(HttpStatus.OK).body(vehicle);
    }

    @GetMapping(path = "/platenumber/{platenumber}")
    public Vehicle findByPlateNumber(@PathVariable(value = "platenumber") String plateNumber){
        return vehicleService.findByPlateNumber(plateNumber);
    }

    @GetMapping("/userid/{userid}")
    public List<Vehicle> findByUserId(@PathVariable(value = "userid") long userid){
        return vehicleService.findByUserId(userid);
    }

}
