package com.application.vehicleRepairShop.utilities;

public enum UserType {
    ADMIN(0),
    USER(1);

    private int code;

    UserType(int code){
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
