package com.application.vehicleRepairShop.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "vehicles", schema = "vehicleproject")
public class Vehicle {

    @Id
    @Column(name = "vehicle_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "color")
    private String color;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;

    @Column(name = "plate_number")
    private String plateNumber;

    public Vehicle(){
    }


    public Vehicle(long id, String brand, String model, LocalDate creationDate, String color, User user, String plateNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.creationDate = creationDate;
        this.color = color;
        this.user = user;
        this.plateNumber = plateNumber;
    }

    public Vehicle(String brand, String model, LocalDate creationDate, String color, User user, String plateNumber) {
        this.brand = brand;
        this.model = model;
        this.creationDate = creationDate;
        this.color = color;
        this.user = user;
        this.plateNumber = plateNumber;
    }

    public Vehicle(long id, String brand, String model, LocalDate creationDate, String color, String plateNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.creationDate = creationDate;
        this.color = color;
        this.plateNumber = plateNumber;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @JsonIgnore
    public User getUser() {
        return user;
    }

    @JsonProperty
    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", creationDate=" + creationDate +
                ", color='" + color + '\'' +
                ", user=" + user +
                ", plateNumber='" + plateNumber + '\'' +
                '}';
    }
}

