package com.application.vehicleRepairShop.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "repairs", schema = "vehicleproject")
public class Repair {

    @Id
    @Column(name = "repair_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "appointment_date")
    private LocalDate appointmentDate;

    @Column(name = "status")
    private String status;

    @Column(name = "service_cost")
    private double cost;

    @Column(name = "vehicle_id")
    private long vehicleId;

    @OneToMany(mappedBy = "repair",cascade = CascadeType.ALL)
    private List<Part> parts;

    public Repair(){
    }

    public Repair(long id, LocalDate appointmentDate, String status, double cost, long vehicleId, List<Part> parts) {
        this.id = id;
        this.appointmentDate = appointmentDate;
        this.status = status;
        this.cost = cost;
        this.vehicleId = vehicleId;
        this.parts = parts;
    }

    public Repair(long id, LocalDate appointmentDate, String status, double cost, long vehicleId) {
        this.id = id;
        this.appointmentDate = appointmentDate;
        this.status = status;
        this.cost = cost;
        this.vehicleId = vehicleId;
        this.parts = new ArrayList<>();
    }

    public Repair(LocalDate appointmentDate, String status, double cost, long vehicleId) {
        this.appointmentDate = appointmentDate;
        this.status = status;
        this.cost = cost;
        this.vehicleId = vehicleId;
        this.parts = new ArrayList<>();
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(LocalDate appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void addPartToList(Part part){
        this.parts.add(part);
    }

    @Override
    public String toString() {
        return "Repair{" +
                "id=" + id +
                ", appointmentDate=" + appointmentDate +
                ", status='" + status + '\'' +
                ", cost=" + cost +
                ", vehicleId=" + vehicleId +
                ", parts=" + parts +
                '}';
    }
}
