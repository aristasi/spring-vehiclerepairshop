package com.application.vehicleRepairShop.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "parts", schema = "vehicleproject")
public class Part {

    @Id
    @Column(name = "part_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "part_name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "cost")
    private double cost;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "repair_id", referencedColumnName = "repair_id")
    private Repair repair;

    public Part(){
    }

    public Part(String name, String type, double cost, Repair repair) {
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.repair = repair;
    }

    public Part(long id, String name, String type, double cost, Repair repair) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.repair = repair;
    }

    public Part(long id, String name, String type, double cost){
        this.id = id;
        this.name = name;
        this.type = type;
        this.cost = cost;
    }

    @JsonIgnore
    public Repair getRepair() {
        return repair;
    }

    @JsonProperty
    public void setRepair(Repair repair) {
        this.repair = repair;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Part part = (Part) o;
        return id == part.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Part{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", cost=" + cost +
                ", repair=" + repair +
                '}';
    }
}
