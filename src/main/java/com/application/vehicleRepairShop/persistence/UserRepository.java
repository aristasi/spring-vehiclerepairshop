package com.application.vehicleRepairShop.persistence;

import com.application.vehicleRepairShop.domain.User;
import com.application.vehicleRepairShop.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findById(long id);

    List<User> findAll();

    void deleteById (long id);

    User findByEmail(String email);

}
