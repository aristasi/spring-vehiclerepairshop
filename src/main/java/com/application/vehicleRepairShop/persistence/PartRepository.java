package com.application.vehicleRepairShop.persistence;

import com.application.vehicleRepairShop.domain.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartRepository extends JpaRepository<Part, Long> {

    List<Part> findAll();

    Part findById(long id);

    List<Part> findByType(String type);

    void deleteById(long id);

}
