package com.application.vehicleRepairShop.persistence;

import com.application.vehicleRepairShop.domain.Repair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepairRepository extends JpaRepository<Repair, Long> {

    List<Repair> findAll();

    Repair findById(long id);

    List<Repair> findByVehicleId (long vehicleId);

    void deleteById (long id);


}
