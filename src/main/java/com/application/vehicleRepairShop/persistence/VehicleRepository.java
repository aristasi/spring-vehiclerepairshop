package com.application.vehicleRepairShop.persistence;

import com.application.vehicleRepairShop.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findAll();

    Vehicle findById(long id);

    Vehicle findByPlateNumber(String plateNumber);

    void deleteById (long id);

    List<Vehicle> findByUserId(long userid);

}
