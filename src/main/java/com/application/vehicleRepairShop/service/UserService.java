package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.User;
import com.application.vehicleRepairShop.domain.Vehicle;

import java.util.List;

public interface UserService {

    User save(User user);

    void deleteById(long id);

    List<User> findAll();

    User findById(long id);

    User findByEmail(String email);

}
