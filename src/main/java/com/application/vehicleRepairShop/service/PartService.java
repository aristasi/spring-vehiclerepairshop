package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.Part;

import java.sql.SQLException;
import java.util.List;

public interface PartService {

    Part save(Part part);

    List<Part> findAll();

    Part findById(long id);

    List<Part> findByType(String type);

    void deleteById(long id);
}
