package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.Part;
import com.application.vehicleRepairShop.domain.Repair;
import com.application.vehicleRepairShop.domain.Vehicle;
import com.application.vehicleRepairShop.persistence.PartRepository;
import com.application.vehicleRepairShop.persistence.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    RepairRepository repairRepository;

    @Autowired
    PartRepository partRepository;


    @Override
    public Repair save(Repair repair) {
       return repairRepository.save(repair);
    }

    @Override
    public List<Repair> findAll() {

        return repairRepository.findAll();
    }

    @Override
    public Repair findById(long id) {

        return repairRepository.findById(id);
    }

    @Override
    public List<Repair> findByVehicleId (long vehicleId) {
        return repairRepository.findByVehicleId(vehicleId);
    }

    @Override
    public void deleteById(long id) {

        repairRepository.deleteById(id);
    }

}

