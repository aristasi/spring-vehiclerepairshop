package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.Part;
import com.application.vehicleRepairShop.persistence.PartRepository;
import com.application.vehicleRepairShop.persistence.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartServiceImpl implements PartService {

    @Autowired
    PartRepository partRepository;

    @Autowired
    RepairRepository repairRepository;


    @Override
    public Part save(Part part) {
        return partRepository.save(part);
    }

    @Override
    public List<Part> findAll() {

        return partRepository.findAll();
    }

    @Override
    public Part findById(long id){

        return partRepository.findById(id);
    }

    @Override
    public List<Part> findByType(String type) {

        return partRepository.findByType(type);
    }

    @Override
    public void deleteById(long id) {

        partRepository.deleteById(id);
    }

}
