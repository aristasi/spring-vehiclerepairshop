package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.Part;
import com.application.vehicleRepairShop.domain.Repair;
import com.application.vehicleRepairShop.domain.Vehicle;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public interface RepairService {

    Repair save(Repair repair);

    List<Repair> findAll();

    Repair findById(long id);

    List<Repair> findByVehicleId (long vehicleId);

    void deleteById (long id);

}
