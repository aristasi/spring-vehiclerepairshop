package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.Repair;
import com.application.vehicleRepairShop.domain.User;
import com.application.vehicleRepairShop.domain.Vehicle;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public interface VehicleService {

    Vehicle save(Vehicle vehicle);

    List<Vehicle> findAll();

    Vehicle findById(long id);

    Vehicle findByPlateNumber(String plateNumber);

    void deleteById(long id);

    List<Vehicle> findByUserId(long userid);

}
