package com.application.vehicleRepairShop.service;

import com.application.vehicleRepairShop.domain.Repair;
import com.application.vehicleRepairShop.domain.User;
import com.application.vehicleRepairShop.domain.Vehicle;
//import com.application.vehicleRepairShop.persistence.VehicleDAO;
//import com.application.vehicleRepairShop.persistence.VehicleDAODBImpl;
//import com.application.vehicleRepairShop.persistence.VehicleDAOImpl;
import com.application.vehicleRepairShop.persistence.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {


    @Autowired
    VehicleRepository vehicleRepository;

    @Override
    public Vehicle save(Vehicle vehicle) {
       return vehicleRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> findAll() {
        return vehicleRepository.findAll();
    }

    @Override
    public Vehicle findById(long id){
        return vehicleRepository.findById(id);
    }

    @Override
    public Vehicle findByPlateNumber(String plateNumber) {
        return vehicleRepository.findByPlateNumber(plateNumber);
    }

    @Override
    public void deleteById(long id) {
        vehicleRepository.deleteById(id);
    }

    @Override
    public List<Vehicle> findByUserId(long userid) {
        return vehicleRepository.findByUserId(userid);
    }

}
