package com.application.vehicleRepairShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleRepairShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleRepairShopApplication.class, args);
	}

}
